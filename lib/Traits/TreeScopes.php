<?php

namespace Molotov\Traits;

use Illuminate\Database\Eloquent\Builder;

trait TreeScopes
{
    public function scopeChildOf(Builder $query, $parentIds, $negative = false) {
        if (!$parentIds) {
            return $query;
        }

        if (!is_array($parentIds)) {
            $parentIds = [$parentIds];
        }

        $table = $this->getTable();
        $binding = join(',', array_map(function ($parentId) {
            return '?';
        }, $parentIds));

        $children = \DB::select(<<<SQL
WITH RECURSIVE tree AS (
    SELECT {$table}.id, {$table}.parent_id
    FROM {$table}
    WHERE parent_id IN ({$binding})

    UNION ALL

    SELECT {$table}.id, {$table}.parent_id
    FROM {$table}
    JOIN tree ON tree.id = {$table}.parent_id
) SELECT * FROM tree;
SQL
            , $parentIds);

        if ($negative) {
            return $query->whereNotIn("{$table}.id", array_pluck($children, 'id'));
        }

        return $query->whereIn("{$table}.id", array_pluck($children, 'id'));
    }

    public function scopeParentOf(Builder $query, $childIds, $negative = false) {
        if (!$childIds) {
            return $query;
        }

        if (!is_array($childIds)) {
            $childIds = [$childIds];
        }

        $table = $this->getTable();
        $binding = join(',', array_map(function ($childId) {
            return '?';
        }, $childIds));

        $parents = \DB::select(<<<SQL
WITH RECURSIVE tree AS (
    SELECT {$table}.id, {$table}.parent_id
    FROM {$table}
    WHERE id IN ({$binding})

    UNION ALL

    SELECT {$table}.id, {$table}.parent_id
    FROM {$table}
    JOIN tree ON tree.parent_id = {$table}.id
) SELECT * FROM tree;
SQL
            , $childIds);

        if ($negative) {
            return $query->whereNotIn("{$table}.id", array_filter(
                array_pluck($parents, 'id'),
                function ($id) use ($childIds) {
                    return !in_array($id, $childIds);
                }
            ));
        }

        return $query->whereIn("{$table}.id", array_filter(
            array_pluck($parents, 'id'),
            function ($id) use ($childIds) {
                return !in_array($id, $childIds);
            }
        ));
    }
}

<?php

namespace Molotov\Traits;

trait RespondsWithErrors
{
    protected static $errorMessages = [
        422 => 'Validation errors',
        404 => 'Not found',
        400 => 'Bad request',
    ];

    protected function respondWithMessage($message = null, $code = null, $status = 400) {
        return response()->json([
            'message' => $message ?: array_get(static::$errorMessages, $status, 'Error'),
            'code' => $code ?: 'error',
            'status' => $status,
        ], $status);
    }

    protected function respondWithErrors($errors, $message = null) {
        return response()->json([
            'message' => $message ?: static::$errorMessages[422],
            'errors' => $errors,
        ], 422);
    }
}

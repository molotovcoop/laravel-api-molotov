<?php

namespace Molotov\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ModelUpdate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model;
    public $before;
    public $after;

    public function __construct($model, $before, $after) {
        $this->model = $model;
        $this->before = $before;
        $this->after = $after;
    }
}

<?php

namespace Molotov\Utils;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Molotov\RestRepository;

class FiltersManager
{
    public static function parseOrderParam($def) {
        $orderBys = [];

        $columns = explode(',', $def);

        foreach ($columns as $column) {
            if (strpos($column, '-') === 0) {
                $direction = 'desc';
                $column = substr($column, 1);
            } else {
                $direction = 'asc';
            }

            if (!isset($orderBys[$column])){
                $orderBys[$column] = $direction;
            }
        }

        return $orderBys;
    }

    protected $connectionQueries = [];
    protected $requiredSqlFields = [];

    protected $reserved = [
        '!fields',
        'fields',
        'for',
        'order',
        'page',
        'per_page',
        'q',
        'schemas',
    ];

    public function addDefinedColumns(&$model, $query, $allColumns = false) {
        $columns = $model->getColumnsDefinition();

        if (count($columns) === 0) {
            $tableName = $model->getTable();
            return $query->selectRaw("{$tableName}.*");
        }

        foreach ($columns as $paramName => $column) {
            if ($allColumns
                || in_array($paramName, $this->requiredSqlFields)
                || $paramName === '*') {
                [$query, $_] = $this->addColumnDefinition($query, $columns, $paramName);
            }
        }

        return $query;
    }

    public function apply(&$model, $param, $value, $query, $request) {
        if (in_array($param, $this->reserved)) {
            return $this->applyReservedParams($model, $param, $value, $query, $request);
        }

        $negative = $param[0] === '!';
        $paramName = str_replace('!', '', $param);
        $columns = $model::getColumnsDefinition();

        if (strpos($paramName, '.') !== false
            && !in_array(str_replace('.', '_', $paramName), array_keys($columns))
        ) {
            [$relation, $field] = explode('.', $paramName, 2);

            if (in_array($relation, array_merge($model->collections, $model->items))) {
                $camelRelation = Str::camel($relation);
                $targetRelation = $model->{$camelRelation}();
                $targetModel = $targetRelation->getRelated();
                $fieldQuery = $negative ? "!$field" : $field;

                // Cannot query accross databases: restart the query and filter by foreign key
                $targetConnectionName = $targetModel->getConnectionName();
                if ($targetConnectionName !== $model->getConnectionName()) {
                    if (is_a($targetRelation, BelongsTo::class)) {
                        $keyName = $targetModel->getKeyName();
                        $foreignKeyName = $targetRelation->getForeignKeyName();
                    } elseif (is_a($targetRelation, HasMany::class)) {
                        $keyName = $targetRelation->getForeignKeyName();
                        $foreignKeyName = $targetRelation->getLocalKeyName();
                    }

                    if (!isset($this->connectionQueries[$targetConnectionName])) {
                        $this->connectionQueries[$targetConnectionName] = [];
                    }

                    if (!isset($this->connectionQueries[$targetConnectionName][$foreignKeyName])) {
                        $this->connectionQueries[$targetConnectionName][$foreignKeyName]
                            = $targetModel->select(\DB::raw("{$keyName} AS id"));
                    }

                    $subquery = $this->connectionQueries[$targetConnectionName][$foreignKeyName];

                    if (is_a($targetRelation, BelongsTo::class) || is_a($targetRelation, HasMany::class)) {
                        $this->connectionQueries[$targetConnectionName][$foreignKeyName]
                            = $subquery->where(
                                function ($q) use ($targetModel, $fieldQuery, $value, $request) {
                                    return $this->apply(
                                        $targetModel,
                                        $fieldQuery,
                                        $value,
                                        $q,
                                        $request
                                    );
                                }
                            );
                        return $query;
                    }
                }

                return $query->where(
                    function ($q) use ($camelRelation, $targetModel, $fieldQuery, $value, $request) {
                        $q = $q->whereHas(
                            $camelRelation,
                            function ($q) use ($targetModel, $fieldQuery, $value, $request) {
                                return $this->apply(
                                    $targetModel,
                                    $fieldQuery,
                                    $value,
                                    $q,
                                    $request
                                );
                            }
                        );

                        if ($fieldQuery[0] === '!') {
                            $q = $q->orDoesntHave($camelRelation);
                        }

                        return $q;
                    }
                );
            } elseif ($this->getFilterType($model, $relation) === 'json') {
                $type = data_get($request->get('schemas'), "$paramName.type");
                $convertedParam = '"' . $query->getModel()->getTable() . '"."'
                    . $relation . '"' . "->>'$field'";

                switch ($type) {
                    case 'textarea':
                    case 'text':
                        return $this->whereText(
                            $model,
                            $query,
                            $convertedParam,
                            $value,
                            $negative
                        );
                    case 'checkbox':
                        $value = (filter_var($value, FILTER_VALIDATE_BOOLEAN) || $value === '')
                            ? \DB::raw("'true'")
                            : \DB::raw("'false'");
                    default:
                        if ($negative) {
                            return $query->where(\DB::raw($convertedParam), '!=', $value);
                        }

                        return $query->where(\DB::raw($convertedParam), $value);
                }

            }

            return $query;
        }

        $camelParamName = Str::camel($paramName);
        if (method_exists($model, "scope$camelParamName")) {
            return $query->{$camelParamName}($value, $negative);
        }

        if (in_array($paramName, array_merge($model->collections, $model->items))) {
            if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
                return $query->doesntHave($camelParamName);
            }

            return $query->whereHas($camelParamName);
        }

        $aggregate = false;
        if (in_array($paramName, array_keys($columns))) {
            [$query, $scopedParam] = $this->addColumnDefinition($query, $columns, $paramName);
            $this->requiredSqlFields[] = $paramName;

            if (preg_match('/(array_agg|sum)\(/i', "$scopedParam")) {
                $aggregate = true;
            }
        } elseif (in_array(str_replace('.', '_', $paramName), array_keys($columns))) {
            [$query, $scopedParam] = $this->addColumnDefinition(
                $query,
                $columns,
                str_replace('.', '_', $paramName)
            );
            $this->requiredSqlFields[] = str_replace('.', '_', $paramName);

            if (preg_match('/(array_agg|sum)\(/i', "$scopedParam")) {
                $aggregate = true;
            }
        } else {
            $scopedParam = "{$query->getModel()->getTable()}.$paramName";
        }

        if ($paramName === 'id' && $value === 'me') {
            $value = $request->user()->id;
        }

        if ($paramName === 'deleted_at' && !!$value) {
            $query = $query->withTrashed();
        }

        switch ($this->getFilterType($model, $paramName)) {
            case 'enum':
                $values = explode(',', $value);
                return $this->applyWhereInFilter(
                    $values,
                    $aggregate,
                    $negative,
                    $scopedParam,
                    $paramName,
                    $query
                );
            case 'boolean':
            case 'checkbox':
                return $query->where(
                    $scopedParam,
                    $negative ? '!=' : '=',
                    filter_var($value, FILTER_VALIDATE_BOOLEAN) || $value === ''
                );
            case 'relation':
            case 'number':
                if (is_array($value)) {
                    $value = implode(',', $value);
                }

                if ($value === '') {
                    return $query;
                }

                if (strpos($value, ':') !== false) {
                    $matches = [];
                    $regex = '(\d*)?';
                    preg_match("/^$regex:$regex$/", $value, $matches);

                    $start = $end = null;
                    if (isset($matches[1])) {
                        $start = $matches[1];
                    }
                    if (isset($matches[2])) {
                        $end = $matches[2];
                    }
                    $range = [$start, $end];

                    return $this->parseRangeFilter($scopedParam, $paramName, $range, $query, $aggregate);
                }

                $values = array_map(
                    'intval',
                    array_map(
                        'trim',
                        array_filter(
                            explode(',', $value),
                            function ($i) {
                                return $i !== '';
                            }
                        )
                    )
                );

                return $this->applyWhereInFilter(
                    $values,
                    $aggregate,
                    $negative,
                    $scopedParam,
                    $paramName,
                    $query
                );
                break;
            case 'text':
                return $this->whereText(
                    $model,
                    $query,
                    str_replace('.', '->', $paramName),
                    $value,
                    $negative
                );
                break;
            case 'date':
            case 'datetime':
                if ($value === '') {
                    return $query;
                }

                $matches = [];
                $dateRegex = '([0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{1,2}:[0-9]{2}(:[0-9]{2})?)?)';
                $intervalRegex = '(P([0-9]+Y)?([0-9]+M)?([0-9]+D)?(T([0-9]+H)?([0-9]+M)?([0-9]+S)?)?)';
                if (preg_match("/^$dateRegex?:$dateRegex?$/", $value, $matches)) {
                    $start = $end = null;
                    if (isset($matches[1])) {
                        $start = $matches[1];
                    }
                    if (isset($matches[4])) {
                        $end = $matches[4];
                    }

                    $tzStart = $start
                        ? Carbon::parse($start)->setTimezone(config('app.timezone'))
                        : null;
                    $tzEnd = $end
                        ? Carbon::parse($end)->setTimezone(config('app.timezone'))
                        : null;
                    $range = [$tzStart, $tzEnd];

                    return $this->parseRangeFilter($scopedParam, $paramName, $range, $query, $aggregate);
                } elseif (preg_match("/^$intervalRegex?:$intervalRegex?$/", $value, $matches)) {
                    $start = $end = null;
                    if (isset($matches[1])) {
                        $start = $matches[1];
                    }
                    if (isset($matches[9])) {
                        $end = $matches[9];
                    }

                    $tzStart = $start ? Carbon::now()
                        ->sub(CarbonInterval::instance(new \DateInterval($start)))
                        ->setTimezone(config('app.timezone')) : null;
                    $tzEnd = $end ? Carbon::now()
                        ->add(CarbonInterval::instance(new \DateInterval($end)))
                        ->setTimezone(config('app.timezone')) : null;
                    $range = [$tzStart, $tzEnd];

                    return $this->parseRangeFilter($scopedParam, $paramName, $range, $query, $aggregate);
                } elseif (preg_match("/^$dateRegex$/", $value, $matches)) {
                    $tzValue = Carbon::parse($value)->setTimezone('UTC');
                    return $query->where($scopedParam, $tzValue);
                }

                // Should not happen, but just pass the value through if nothing matches
                return $query->where($scopedParam, $value);
                break;
            default:
                if ($negative) {
                    if ($aggregate) {
                        return $query->having($scopedParam, '!=', $value);
                    }
                    return $query->where($scopedParam, '!=', $value);
                }

                if ($aggregate) {
                    return $query->having($scopedParam, $value);
                }
                return $query->where($scopedParam, $value);
                break;
        }

        foreach ($this->connectionQueries as $connection => $queries) {
            foreach ($queries as $foreignKeyName => $subquery) {
                $rawSql = RestRepository::queryToRawSql($subquery);

                $keys = array_map(
                    function ($obj) {
                        return $obj->id;
                    },
                    \DB::connection($connection)->select($rawSql)
                );

                $query = $query->whereIn($foreignKeyName, $keys);
            }
        }

        return $query;
    }

    public function applyReservedParams(&$model, $param, $value, $query, $request) {
        $negative = $param[0] === '!';
        $paramName = str_replace('!', '', $param);

        if ($paramName === 'q' && method_exists($model, 'scopeSearch')) {
            return $query->search($value, $negative);
        }

        if ($paramName === 'order') {
            return $this->orderBy($model, $query, $value);
        }

        if ($fields = $request->getFields()) {
            return $this->applyWithFromQuery($model, $fields, $query);
        }

        return $query;
    }

    protected function applyWithFromQuery(&$model, $fields, $query) {
        $relations = array_merge(
            $model->items,
            $model->collections,
            array_keys($model->morphOnes)
        );
        foreach (array_keys($fields) as $field) {
            if (in_array($field, $relations)) {
                $query = $query->with(Str::camel($field));
            }
        }

        return $query;
    }

    protected function orderBy(&$model, $query, $def) {
        if ($def === '') {
            return $query;
        }

        $orderBys = static::parseOrderParam($def);

        foreach ($orderBys as $column => $direction) {
            $parts = explode('.', $column, 2);

            if (count($parts) === 2) {
                [$relation, $field] = $parts;
            } else {
                $relation = null;
            }

            $allRelations = array_merge($model->collections, $model->items);
            if (in_array($relation, $allRelations)) {
                $camelRelationName = Str::camel($relation);
                $targetRelation = $model->{$camelRelationName}();
                $targetRelated = $targetRelation->getRelated();
                $definitions = $targetRelated->getColumnsDefinition();

                if (is_callable($definitions[$field])) {
                    $query = $query->orderBy(\DB::raw($definitions[$field]()), $direction);
                } elseif (is_string($definitions[$field])) {
                    $query = $query->leftJoin(
                        $targetRelated->getTable(),
                        $targetRelation->getQualifiedForeignKeyName(),
                        '=',
                        $targetRelation->getQualifiedOwnerKeyName(),
                    );
                    $query = $query->orderBy(\DB::raw($definitions[$field]), $direction);
                }
            } elseif (in_array($column, array_keys($model->getColumnsDefinition()))) {
                $query = $query->orderBy($column, $direction);
                $this->requiredSqlFields[] = $column;
            } else {
                $table = $model->getTable();
                $query = $query->orderBy("$table.$column", $direction);
            }
        }

        return $query;
    }

    protected function parseRangeFilter($scopedParam, $paramName, $range, &$query, $aggregate = false) {
        [$start, $end] = array_map(function ($r) {
            if ($r === '' || $r === null) {
                return null;
            }

            return $r;
        }, $range);

        if ($start === null && $end === null) {
            return $query;
        } elseif ($start === null) {
            if ($aggregate) {
                return $query->having($paramName, '<=', $end);
            }
            return $query->where($scopedParam, '<=', $end);
        } elseif ($end === null) {
            if ($aggregate) {
                return $query->having($paramName, '>=', $start);
            }
            return $query->where($scopedParam, '>=', $start);
        }

        if ($aggregate) {
            if ($aggregate) {
                return $query->having($paramName, '>=', $start)
                    ->having($paramName, '<=', $end);
            }
        }

        return $query->whereBetween($scopedParam, [$start, $end]);
    }

    private function addColumnDefinition($query, $definition, $column) {
        $currentColumns = array_map(function ($c) {
            return "$c";
        }, $query->getQuery()->columns ?: []);

        if (is_callable($definition[$column])) {
            $scopedParam = \DB::raw($definition[$column]());
            $sql = $definition[$column]();
            if (!in_array("{$sql} AS $column", $currentColumns)) {
                $query = $definition[$column]($query);
            }
        } else {
            $scopedParam = \DB::raw($definition[$column]);
            if (!in_array("{$definition[$column]} AS $column", $currentColumns)) {
                $query = $query->selectRaw("{$definition[$column]} AS $column");
            }
        }

        return [$query, $scopedParam];
    }

    private function applyWhereInFilter(
        $values,
        $aggregate,
        $negative,
        $scopedParam,
        $paramName,
        &$query
    ) {
        if ($aggregate) {
            $placeholders = implode(',', array_map(function () {
                return '?';
            }, $values));

            if ($negative) {
                return $query->whereNot(
                    function ($q) use ($scopedParam, $placeholders, $values) {
                        return $q->havingRaw("$paramName in ($placeholders)", $values);
                    }
                );
            }

            return $query->havingRaw("$paramName in ($placeholders)", $values);
        }

        if ($negative) {
            return $query->whereNotIn($scopedParam, $values);
        }

        return $query->whereIn($scopedParam, $values);
    }

    private function getFilterType(&$model, $paramName) {
        // If a type is defined for this filter, use the query
        // language, otherwise fallback to default Laravel filtering
        $filterType = array_get($model::$filterTypes, "$paramName.type")
            ?: array_get($model::$filterTypes, $paramName)
            ?: array_get($model::$filterTypes, str_replace('.', '_', $paramName));

        if (is_array($filterType)) {
            return 'enum';
        }

        if (!$filterType && $paramName === 'id') {
            return 'number';
        }

        return $filterType;
    }

    private function whereText(&$model, $query, $scopedParam, $value, $negative = false) {
        $matches = [];
        if (preg_match('/^"(.*)"$/', $value, $matches)) {
            if ($negative) {
                return $query->where($scopedParam, '!=', $matches[1]);
            }

            return $query->where($scopedParam, '=', $matches[1]);
        }

        if ($negative) {
            if ($model->getConnection()->getConfig()['driver'] === 'pgsql') {
                $escapedValue = pg_escape_string($value);
                return $query->where(function ($q) use ($scopedParam, $value) {
                    return $q->where(
                        \DB::raw("unaccent($scopedParam)"),
                        'NOT ILIKE',
                        \DB::raw("unaccent('%$escapedValue%')")
                    )->orWhereNull($scopedParam);
                });
            } else {
                return $query->where($scopedParam, 'NOT LIKE', "%$value%");
            }
        }

        if ($model->getConnection()->getConfig()['driver'] === 'pgsql') {
            $escapedValue = pg_escape_string($value);
            return $query->where(
                \DB::raw("unaccent($scopedParam)"),
                'ILIKE',
                \DB::raw("unaccent('%$escapedValue%')")
            );
        } else {
            return $query->where($scopedParam, 'LIKE', "%$value%");
        }
    }
}

<?php

namespace Molotov;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Molotov\Traits\ParsesFields;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class Export extends DefaultValueBinder implements FromCollection, WithHeadings, WithMapping, WithCustomCsvSettings, WithCustomValueBinder
{
    use Exportable, ParsesFields;

    protected $items;
    protected $fields;
    protected $format;
    protected $model;

    public function __construct($items, $fields, $model, string $format) {
        $this->items = $items;
        $this->model = $model;
        $this->fields = $this->explodeFields($fields, $model);
        $this->format = $format;
    }

    public function bindValue(Cell $cell, $value) {
        if ($value instanceof \stdClass || is_array($value)) {
            return json_encode($value);
        }

        return parent::bindValue($cell, $value);
    }

    public function headings(): array {
        return $this->fields;
    }

    public function map($item): array {
        return  array_map(function ($f) use ($item) {
            return data_get($item, $f);
        }, $this->fields);
    }

    public function collection() {
        return $this->items;
    }

    protected function explodeFields($fields, $model, $parent = null) {
        $explodedFields = [];
        $relationsCount = [];
        $relationsFields = [];

        $parsedFields = $this->parseFields($this->splitFields(implode(',', $fields)));

        foreach ($parsedFields as $name => $rest) {
            if (!is_array($rest)) {
                $explodedFields[] = $rest;
                continue;
            }

            if (in_array(
                $name,
                array_merge($model->items, $model->schemas, array_keys($model->morphOnes))
            )) {
                $explodedFields = array_merge(
                    $explodedFields,
                    $this->joinFieldsTree($rest, $name)
                );
            } elseif (in_array(
                $name,
                array_merge($model->collections, array_keys($model->morphManys))
            )) {
                if (!isset($relationsCount[$name])) {
                    $largestOccurence = $this->items->reduce(
                        function ($acc, $i) use ($name) {
                            $target = array_get($i, $name);
                            if ((is_array($target) || is_a($target, Collection::class))
                                && count($target) > $acc) {
                                return count($target);
                            }

                            return $acc;
                        },
                        1
                    );
                    $relationsCount[$name] = $largestOccurence;
                }

                if (!isset($relationsFields[$name])) {
                    $relationsFields[$name] = [];
                }

                $camelName = Str::camel($name);
                $subfields = $this->explodeFields(
                    $this->joinFieldsTree($rest),
                    $model->{$camelName}()->getRelated(),
                    $model->{$camelName}()
                );
                $relationsFields[$name] = $subfields;
            } elseif (!!$parent && $pivotClass = $parent->getPivotClass()) {
                $pivot = new $pivotClass;
                if (in_array(
                    $name,
                    array_merge($pivot->items, array_keys($pivot->morphOnes))
                )) {
                    $explodedFields = array_merge(
                        $explodedFields,
                        $this->joinFieldsTree($rest, $name)
                    );
                } elseif (in_array(
                    $name,
                    array_merge(
                        $pivot->collections,
                        array_keys($pivot->morphManys)
                    )
                )) {
                    if (!isset($relationsCount[$name])) {
                        $largestOccurence = $this->items->reduce(
                            function ($acc, $i) use ($name) {
                                $target = array_get($i, $name);
                                if ((is_array($target) || is_a($target, Collection::class))
                                    && count($target) > $acc) {
                                    return count($target);
                                }

                                return $acc;
                            },
                            1
                        );
                        $relationsCount[$name] = $largestOccurence;
                    }

                    if (!isset($relationsFields[$name])) {
                        $relationsFields[$name] = [];
                    }

                    $camelName = Str::camel($name);
                    $subfields = $this->explodeFields(
                        $this->joinFieldsTree($rest),
                        $pivot->{$camelName}()->getRelated()
                    );
                    $relationsFields[$name] = $subfields;
                }
            }
        }

        foreach (array_keys($relationsCount) as $name) {
            for ($i = 0; $i < $relationsCount[$name]; $i++) {
                foreach ($relationsFields[$name] as $field) {
                    $explodedFields[] = "$name.$i.$field";
                }
            }
        }

        return $explodedFields;
    }

    public function getCsvSettings(): array {
        switch ($this->format) {
            case 'tsv':
                return [
                    'delimiter' => "\t",
                ];
            case 'csv':
            default:
                return [
                    'delimiter' => ',',
                ];
        }
    }
}

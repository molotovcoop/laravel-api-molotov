<?php

namespace Molotov\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class WrapInTransaction
{
    public function handle($request, Closure $next, $guard = null) {
        DB::beginTransaction();

        $response = $next($request);

        // Error response
        $status = (int) $response->getStatusCode();
        if (in_array($status, [400, 500])) {
            return $response;
        }

        DB::commit();

        return $response;
    }
}

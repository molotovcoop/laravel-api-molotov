<?php

namespace Molotov\Utils;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;

class RouteHelper
{
    public static function readOnlyResource($slug, $controller = null) {
        self::resource($slug, $controller, [
            'create',
            'destroy',
            'restore',
            'update',
        ]);
    }

    public static function writeOnlyResource($slug, $controller = null) {
        self::resource($slug, $controller, [
            'index',
            'retrieve',
            'template',
        ]);
    }

    public static function resource($slug, $controller = null, $except = [], $options = []) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);
        $nameSlug = data_get($options, 'name', $pluralSlug);

        $controller = $controller ?: ucfirst("{$camelSlug}Controller");

        Route::prefix(str_replace('-', '_', $pluralSlug))->group(
            function () use ($controller, $pluralSlug, $nameSlug, $except) {
                if (!in_array('index', $except)) {
                    static::indexRoute($controller, $pluralSlug, "$nameSlug.index");
                }

                if (!in_array('create', $except)) {
                    Route::post('/', "$controller@create")
                        ->name("$nameSlug.create");
                }

                if (!in_array('restore', $except)) {
                    Route::put('/{id}/restore', "$controller@restore")
                        ->where('id', '[0-9]+')
                        ->name("$nameSlug.restore");
                }

                if (!in_array('retrieve', $except)) {
                    static::retrieveRoute($controller, $pluralSlug, "$nameSlug.retrieve");
                }

                if (!in_array('update', $except)) {
                    Route::put('/{id}', "$controller@update")
                        ->where('id', '[0-9]+')
                        ->name("$nameSlug.update");
                }

                if (!in_array('destroy', $except)) {
                    Route::delete('/{id}', "$controller@destroy")
                        ->where('id', '[0-9]+')
                        ->name("$nameSlug.destroy");
                }

                if (!in_array('template', $except)) {
                    Route::options('/', "$controller@template")
                        ->name("$nameSlug.template");
                }
            }
        );
    }

    public static function index($slug, $options = []) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $routeName = array_get($options, 'name') ?: "$pluralSlug.index";

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(
            function () use ($controller, $pluralSlug, $routeName) {
                static::indexRoute($controller, $pluralSlug, $routeName);
            }
        );
    }

    private static function indexRoute($controller, $pluralSlug, $routeName) {
        Route::get('/', "$controller@index")->name($routeName);
    }

    public static function retrieve($slug, $options = []) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $routeName = array_get($options, 'name') ?: "$pluralSlug.retrieve";

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(
            function () use ($controller, $pluralSlug, $routeName) {
                static::retrieveRoute($controller, $pluralSlug, $routeName);
            }
        );
    }

    private static function retrieveRoute($controller, $pluralSlug, $routeName) {
        Route::get('/{id}', "$controller@retrieve")
            ->where('id', '[0-9]+')
            ->name($routeName);
    }

	public static function subresource(
		$slugs,
		$controller = null,
		$except = [],
		$params = []
	) {
		$slugs = explode('.', $slugs);
		$prefix = '';

		while (count($slugs) > 1) {
			$parentSlug = array_shift($slugs);
			$params[] = $parentSlug;
			$slug = $slugs[0];
			$parentPluralSlug = Pluralizer::plural($parentSlug);

			$parentCamelSlug = Str::camel(str_replace('-', '_', $parentSlug));
			$controller = $controller ?: ucfirst("{$parentCamelSlug}Controller");

			$prefix .= "/$parentPluralSlug/{{$parentSlug}_id}";

			Route::prefix($prefix)
				->group(function () use (
					$slug,
					$parentSlug,
					$parentPluralSlug,
					$slugs,
					$controller,
					$except,
					$params,
					$prefix
				) {
					$pluralSlug = Pluralizer::plural($slug);

					if (count($slugs) > 1) {
						$prefix .= "/$pluralSlug/{{$slug}_id}";
					} else {
						$params[] = $pluralSlug;
						$methodSuffix = ucfirst(Str::Camel(implode('_', $params)));

						if (!in_array('index', $except)) {
							$route = Route::get(
								"/$pluralSlug",
								"$controller@index{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'index');
						}

						if (!in_array('create', $except)) {
							$route = Route::post(
								"/$pluralSlug",
								"$controller@create{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'create');
						}

						if (!in_array('retrieve', $except)) {
							$route = Route::get(
								"/$pluralSlug/{{$slug}_id}",
								"$controller@retrieve{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'retrieve');
						}

						if (!in_array('update', $except)) {
							$route = Route::put(
								"/$pluralSlug/{{$slug}_id}",
								"$controller@update{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'update');
						}

						if (!in_array('destroy', $except)) {
							$route = Route::delete(
								"/$pluralSlug/{{$slug}_id}",
								"$controller@destroy{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'destroy');
						}


						if (!in_array('restore', $except)) {
							$route = Route::put(
								"/$pluralSlug/{{$slug}_id}/restore",
								"$controller@restore{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'restore');
						}

						if (!in_array('template', $except)) {
							$route = Route::options(
								"/$pluralSlug",
								"$controller@template{$methodSuffix}"
							);

							static::setRouteParamsAndName($route, $params, 'template');
						}
					}
				}
			);
		}
    }

	private static function setRouteParamsAndName($route, array $params, string $name) {
		foreach ($params as $param) {
			$route = $route->where("{$param}_id", '[0-9]+');
		}

		$allParams = implode('__', $params);
		$route->name("{$allParams}.$name");
	}
}

<?php

namespace Molotov;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Molotov\Traits\ParsesFields;

class Request extends FormRequest
{
    use ParsesFields;

    protected static function renameRule(&$rules, $from, $to) {
        $rules[$to] = $rules[$from];
        unset($rules[$from]);
    }

    protected static function rebaseRules($scope, $rules) {
        return array_reduce(
            array_keys($rules),
            function ($acc, $key) use ($rules, $scope) {
                $acc[$scope . '.' . $key] = $rules[$key];
                return $acc;
            },
            []
        );
    }

    private $fieldsMemo = null;
    private $notFieldsMemo = null;

    private $context = [];

    public function authorize() {
        return true;
    }

    public function rules() {
        return [];
    }

    public function messages() {
        return [];
    }

    public function attributes() {
        return [];
    }

    public function getFields() {
        if ($this->fieldsMemo !== null) {
            return $this->fieldsMemo;
        }

        if (!$this->get('fields')) {
            return $this->fieldsMemo = ['*' => '*'];
        }

        return $this->fieldsMemo = $this->parseFields(
            $this->splitFields($this->get('fields'))
        );
    }

    public function getNotFields() {
        if ($this->notFieldsMemo !== null) {
            return $this->notFieldsMemo;
        }

        if (!$this->get('!fields')) {
            return $this->notFieldsMemo = [];
        }

        return $this->notFieldsMemo = $this->parseFields(
            $this->splitFields($this->get('!fields'))
        );
    }

    public function getAuthorizedInput($user = null) {
        return $this->json()->all();
    }

    public function mergeJson($input) {
        $json = $this->json();
        $json->add($input);
    }

    public function redirectAs($user, $requestClass = null) {
        $class = $requestClass ? $requestClass : get_class($this);
        $newRequest = new $class;
        $newRequest->setUserResolver(function () use ($user) {
            return $user;
        });
        if (!$newRequest->authorize()) {
            return abort(403);
        }
        return $newRequest;
    }

    public function redirectAuth($requestClass = null, $data = null) {
        $class = $requestClass ? $requestClass : get_class($this);
        $newRequest = new $class;
        $newRequest->setUserResolver(function () {
            return $this->user();
        });

        if ($data) {
            $newRequest->merge($data);
            $newRequest->setJson(collect($data));
        }

        if (!$newRequest->authorize()) {
            return abort(403);
        }
        return $newRequest;
    }

    public function redirect($requestClass) {
        $newRequest = new $requestClass;
        $newRequest->setUserResolver(function () {
            return $this->user();
        });
        $newRequest->setRouteResolver(function () {
            return $this->route();
        });
        $newRequest->merge($this->all());
        $newRequest->setJson($this->json());

        if (!$newRequest->authorize()) {
            return abort(403);
        }

        $validator = Validator::make(
            $newRequest->all(),
            $newRequest->rules(),
            $newRequest->messages(),
            $newRequest->attributes()
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $newRequest;
    }

    public function extractField(...$fields) {
        $extraction = $this->only($fields);

        foreach ($fields as $field) {
            $this->json()->remove($field);
        }

        return $extraction;
    }

    public function getContext() {
        return $this->context;
    }

    public function setContext($context) {
        $this->context = $context;
    }

    public function addContext($key, $value) {
        $this->context[$key] = $value;
    }

    public function removeContext($key) {
        unset($this->context[$key]);
    }

    public function queryStringAll() {
        return $this->parseQueryString(array_get($this->server->all(), 'QUERY_STRING'))
            ?: $this->all();
    }

    // PHP's default query string parser replaces . by _
    // which would break our query language
    private function parseQueryString($data) {
        $data = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function ($match) {
            return bin2hex(urldecode($match[0]));
        }, $data);

        parse_str($data, $values);

        return array_combine(array_map('hex2bin', array_keys($values)), $values);
    }
}

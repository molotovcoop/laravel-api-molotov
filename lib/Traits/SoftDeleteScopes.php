<?php

namespace Molotov\Traits;

use Illuminate\Database\Eloquent\Builder;

trait SoftDeleteScopes
{
    public function scopeIsDeleted(Builder $query, $value, $negative = false) {
        if ($value && $negative) {
            return $query->withTrashed()->whereNull('deleted_at');
        }

        if ($value || $negative){
            return $query->withTrashed()->whereNotNull('deleted_at');
        }

        return $query->withTrashed()->whereNull('deleted_at');
    }

    public function scopeWithDeleted(Builder $query, $value, $negative = false) {
        if ($value && $negative) {
            return $query;
        }

        if ($value || $negative){
            return $query->withTrashed();
        }

        return $query;
    }

    public function scopeDeletedAt(Builder $query, $value, $negative = false) {
        return $query->withTrashed();
    }
}

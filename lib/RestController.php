<?php

namespace Molotov;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Excel;
use Molotov\Traits\RespondsWithErrors;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RestController extends Controller
{
    use RespondsWithErrors;

    protected $repo;
    protected $model;

    protected function respondWithCollection($request, $items, $total) {
        $perPage = $request->get('per_page') ?: 10;
        if ($perPage === '*') {
            $perPage = -1;
        }
        $page = $request->get('page') ?: 1;

        if ($items->isEmpty()) {
            return new LengthAwarePaginator([], $total, $perPage, $page);
        }

        $results = $this->transformCollection($request, $items)->values();

        return new LengthAwarePaginator($results, $total, $perPage, $page);
    }

    protected function respondWithCsv($request, $items, $model) {
        return $this->respondWith('csv', $request, $items, $model);
    }

    protected function respondWithTsv($request, $items, $model) {
        return $this->respondWith('tsv', $request, $items, $model);
    }

    protected function respondWithXls($request, $items, $model) {
        return $this->respondWith('xls', $request, $items, $model);
    }

    protected function respondWithXlsx($request, $items, $model) {
        return $this->respondWith('xlsx', $request, $items, $model);
    }

    protected function respondWithTemplate($request, $model) {
        $modelRules = $model->getRules('template');
        $form = [];

        foreach ($modelRules as $field => $rules) {
            if (!isset($form[$field])) {
                $form[$field] = [];
            }

            if (substr($field, -3) === '_id' && !isset($model::$filterTypes[$field])) {
                $form[$field]['type'] = 'relation';
                $filterKey = substr($field, 0, -3) . '.id';

                if (isset($model::$filterTypes[$filterKey])) {
                    $form[$field] = $model::$filterTypes[$filterKey];
                }
            } elseif ($def = $this->getDefFromFilters($model::$filterTypes, $field)) {
                $form[$field] = $def;
            } elseif ($type = $this->getTypeFromRules($rules)) {
                $form[$field]['type'] = $type;
            }
            $form[$field]['rules'] = $this->formatRules($rules);
        }

        $filters = [];
        foreach ($model::$filterTypes as $name => $filter) {
            if (is_string($filter)) {
                $filters[$name] = [
                    'type' => $filter,
                ];
            } else {
                $filters[$name] = $filter;
            }
        };

        if (method_exists($this->model, 'scopeSearch')) {
            $filters['q'] = [
                'type' => 'text',
            ];
        }

        $item = new \stdClass;

        foreach ($model->collections as $collection) {
            $item->{$collection} = [];
        }

        return [
            'item' => $item,
            'form' => $form ?: new \stdClass,
            'filters' => $filters ?: new \stdClass,
        ];
    }

    protected function validateAndCreate($request) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('create', $input),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $item = $this->repo->create($request, $input);

        $className = get_class($request);
        $fieldsRequest = new $className;
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge([ 'fields' => $request->get('fields') ]);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function respondWithItem($request, $item, $status = 200) {
        $result = $this->transformItem($request, $item);

        return response($result, $status);
    }

    protected function validateAndUpdate($request, $id) {
        $className = get_class($request);
        $fieldsRequest = new $className;
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge([
            'fields' => $request->get('fields'),
            'for' => 'update',
        ]);

        $item = $this->repo->find($fieldsRequest, $id);

        $input = $request->getAuthorizedInput($request->user());
        $item->fill($input);

        foreach (array_merge($item->items, array_keys($item->morphOnes)) as $relation) {
            if (isset($input[$relation])) {
                $item->{$relation} = $input[$relation];
            }
        }

        $validator = Validator::make(
            $item->toArray(),
            $this->model::getRules('update', $input),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $item = $this->repo->update($request, $id, $input);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function validateAndDestroy($request, $id) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('destroy', $input),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->repo->destroy($request, $id);
    }

    protected function validateAndRestore($request, $id) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('destroy', $input),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->repo->restore($request, $id);
    }

    protected function streamFile($callback, $headers) {
        $response = new StreamedResponse($callback, 200, $headers);
        $response->send();
    }

    protected function getTypeFromRules($rules) {
        if (is_string($rules)) {
            $rules = explode('|', $rules);
        }

        $cleanRules = [];
        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }
            $cleanRules[] = $rule;
        }

        foreach ($cleanRules as $rule) {
            $ruleParts = explode(':', $rule);
            $ruleName = $ruleParts[0];

            switch ($ruleName) {
                case 'numeric':
                    return 'number';
                case 'string':
                    return 'text';
                case 'text':
                    return 'textarea';
                case 'date':
                case 'email':
                    return $ruleName;
                case 'boolean':
                    return 'checkbox';
            }
        }
    }

    protected function getDefFromFilters($filters, $field) {
        if (isset($filters[$field])) {
            if (is_string($filters[$field])) {
                return [
                    'type' => $filters[$field],
                ];
            }

            if (isset($filters[$field]['type'])) {
                return $filters[$field];
            }
        }

        return null;
    }

    protected function formatRules($rules) {
        if (is_string($rules)) {
            $rules = explode('|', $rules);
        }

        $cleanRules = [];
        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }
            $cleanRules[] = $rule;
        }

        $flipRules = array_flip($cleanRules);

        foreach ($flipRules as $key => $rule) {
            if (strpos($key, 'in:') === 0) {
                unset($flipRules[$key]);
                [$_, $values] = explode(':', $key);
                $flipRules['oneOf'] = explode(',', $values);
            } elseif (strpos($key, ':')) {
                unset($flipRules[$key]);

                [$rule, $values] = explode(':', $key);
                $flipRules[$rule] = explode(',', $values);
                if (count($flipRules[$rule]) === 1) {
                    $flipRules[$rule] = array_pop($flipRules[$rule]);
                }

                switch ($rule) {
                    case 'exists':
                        unset($flipRules[$rule]);
                        break;
                    case 'regex':
                        $flipRules[$rule] = substr($flipRules[$rule], 1, -1);
                        break;
                    case 'digits':
                        $flipRules[$rule] = (int) $flipRules[$rule];
                        break;
                    case 'size':
                        $flipRules['max'] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    case 'max':
                        $flipRules['max_value'] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    default:
                        break;
                }
            } else {
                $flipRules[$key] = true;
            }
        }

        return $flipRules;
    }

    protected function transformCollection($request, $items) {
        return $items->map(function ($item) use ($request) {
            return $this->transformItem($request, $item);
        });
    }

    protected function transformItem($request, $item) {
        $transformer = new $item::$transformer;

        $reflect = new \ReflectionClass($this->model);
        $shortName = $reflect->getShortName();
        $context = $request->getContext();
        $params = $request->all();
        $context[$shortName] = true;

        return $transformer->transform($item, [
            'fields' => $request->getFields(),
            '!fields' => $request->getNotFields(),
            'pivot' => isset($item->pivot) ? $item->pivot : null,
            'params' => $params,
            'context' => $context,
        ]);
    }

    private function respondWith(string $format, $request, $items, $model): string {
        if (is_a($items, LazyCollection::class)) {
            $item = $items->first();
        } else {
            $item = $items[0];
        }

        $userId = $request->user()->id;
        $date = date('YmdHmi');
        $modelClassName = get_class($model);
        $modelClassNameParts = explode('\\', $modelClassName);
        $modelName = array_pop($modelClassNameParts);
        $filename = Str::plural(strtolower($modelName));

        $path = "/exports/$date.$userId.$filename.$format";

        switch ($format) {
            case 'csv':
                $fields = $request->get('fields')
                    ? explode(',', $request->get('fields'))
                    : $this->model->getFillable();
                if ($request->user()->superadmin) {
                    $collection = $items;
                } else {
                    $collection = $this->transformCollection($request, $items);
                }

                $file = fopen(storage_path("app/$path"), 'w');
                fputcsv($file, $fields);

                foreach ($collection as $item) {
                    fputcsv(
                        $file,
                        array_map(function ($f) use ($item) {
                            return (string) data_get($item, $f);
                        }, $fields)
                    );
                }

                fclose($file);
                break;
            default:
                $fields = $request->get('fields')
                    ? explode(',', $request->get('fields'))
                    : $this->model->getFillable();
                $export = new $item::$export(
                    $this->transformCollection($request, $items),
                    $fields,
                    $model,
                    $format
                );

                $userId = $request->user()->id;
                $date = date('YmdHmi');
                $modelClassName = get_class($model);
                $modelClassNameParts = explode('\\', $modelClassName);
                $modelName = array_pop($modelClassNameParts);
                $filename = Str::plural(strtolower($modelName));

                $path = "/exports/$date.$userId.$filename.$format";

                $export->store($path, 'local');
                break;
        }


        return $path;
    }
}

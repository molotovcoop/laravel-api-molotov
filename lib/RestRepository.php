<?php

namespace Molotov;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\PostgresConnection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Molotov\Events\ModelUpdate;
use Molotov\Utils\FiltersManager;

class RestRepository
{
    public static function queryToRawSql($query) {
        $sql = $query->toSql();

        $bindings = array_map(function ($p) {
            if (is_bool($p)) {
                return $p ? "'t'" : "'f'";
            }
            $escapedP = pg_escape_string($p);
            return "'$escapedP'";
        }, $query->getBindings());

        $sql = str_replace('%', '~!~!~', $sql);
        $sql = str_replace('?', '%s', $sql);
        $subquery = vsprintf($sql, $bindings);
        $subquery = str_replace('~!~!~', '%', $subquery);

        return $subquery;
    }

    protected $model;
    protected $filters;

    protected $after = [];
    protected $before = [];

    public function __construct($model) {
        $this->model = $model;
        $this->filters = new FiltersManager();
    }

    public function get($request) {
        $query = $this->model;

        if (method_exists($this->model, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        if (method_exists($this->model, 'scopeFor')) {
            $query = $query->for($request->get('for'), $request->user());
        }

        $params = $request->queryStringAll();

        $extraParams = array_diff_key($request->all(), $params);
        $paramKeys = array_keys($params);
        foreach ($extraParams as $extraParam => $value) {
            if (strpos($extraParam, '_')
                && !in_array($extraParam, array_keys($this->model::$filterTypes))) {
                $parts = explode('_', $extraParam);
                for ($i = 1; $i < count($parts); $i++) {
                    $relation = implode('_', array_slice($parts, 0, $i));
                    $rest = implode('_', array_slice($parts, $i));

                    if (in_array(
                        $relation,
                        array_merge($this->model->collections, $this->model->items, $this->model->schemas),
                    )) {
                        continue 2;
                    }

                    if (in_array("$relation.$rest", $paramKeys)) {
                        continue 2;
                    }
                }
            }

            $params[$extraParam] = $value;
        }

        foreach ($params as $param => $value) {
            $query = $this->filters->apply($this->model, $param, $value, $query, $request);
        }

        $query = $this->filters->addDefinedColumns($this->model, $query);

        // TODO Move to controller
        $perPage = $request->get('per_page') ?: 10;
        $page = $request->get('page') ?: 1;

        if (strpos($query->toSql(), 'group by') !== false) {
            $total = $this->wrapQueryForRealTotal($this->model->getConnectionName(), $query);
        } else {
            try {
                $total = $query->count();
            } catch (\Illuminate\Database\QueryException $e) {
                $total = $this->wrapQueryForRealTotal($this->model->getConnectionName(), $query);
            }
        }

        if ($perPage === '*') {
            $perPage = -1;
        } else {
            $query = $query->take($perPage)->skip($perPage * ($page - 1));
        }

        try {
            if (empty($query->getEagerLoads())) {
                $items = $query->cursor();
            } else {
                $items = $query->get();
            }
        } catch (RelationNotFoundException $e) {
            throw new ValidationException([
                'includes' => 'relationship does not exist',
            ]);
        }

        return [$items, $total];
    }

    public function find($request, $id) {
        if (!intval($id)) {
            return abort(422, 'Numeric ids.');
        }

        $query = $this->model;

        if (method_exists($this->model, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        if (method_exists($this->model, 'scopeFor')) {
            $query = $query->for($request->get('for'), $request->user());
        }

        $params = $request->queryStringAll();
        foreach ($params as $param => $value) {
            $query = $this->filters->apply($this->model, $param, $value, $query, $request);
        }

        $query = $this->filters->addDefinedColumns($this->model, $query, true);

        return $query->findOrFail($id);
    }

    public function create($request, $data) {
        $this->model->fill($data);

        $this->after = array_merge($this->after, $this->model->getDirty());

        $this->model->save();

        $this->saveRelations($data, $request);

        $this->model->save();

        $model = $this->model;

        $className = get_class($this->model);
        $this->model = new $className;

        ModelUpdate::dispatch($model, $this->before, $this->after);

        return $model;
    }

    public function update($request, $id, $data) {
        $query = $this->model;

        if (method_exists($this->model, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        if (method_exists($this->model, 'scopeFor')) {
            $query = $query->for($request->get('for'), $request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->fill($data);

        $this->before = array_merge($this->before, $this->model->getOriginal());
        $this->after = array_merge($this->after, $this->model->getDirty());

        $this->model->save();

        $this->saveRelations($data, $request);

        $this->model->save();

        $model = $this->model;

        $className = get_class($this->model);
        $this->model = new $className;

        ModelUpdate::dispatch($model, $this->before, $this->after);

        return $this->model->find($id);
    }

    public function destroy($request, $id) {
        $query = $this->model;

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->delete();

        return $this->model;
    }

    public function restore($request, $id) {
        $query = $this->model->withTrashed();

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->restore();

        return $this->model;
    }

    protected function saveRelations($data, $request) {
        $this->saveItems($data, $request);
        $this->saveCollections($data, $request);
    }

    protected function saveItems($data, $request) {
        $skipObjectRelation = [];
        foreach (array_diff(array_keys($data), $this->model->getFillable()) as $field) {
            if ($field === 'id') {
                continue;
            }

            if (preg_match('/_id$/', $field)) {
                $relationName = str_replace('_id', '', $field);

                if (in_array($relationName, $skipObjectRelation)) {
                    continue;
                }

                if (in_array($relationName, $this->model->items)) {
                    $newAssoc = $data[$field];

                    $skipObjectRelation[] = $relationName;

                    $camelRelationName = Str::camel($relationName);
                    $relation = $this->model->{$camelRelationName}();

                    if (is_a($relation, BelongsTo::class)) {
                        if ($relationBefore = $relation->first()) {
                            $this->before[$relationName] = $relationBefore->toArray();
                        }

                        if ($newAssoc) {
                            $relation->associate($data[$field]);

                            $this->after[$relationName] = $relation->getRelated()
                                ->find($data[$field]);
                        } else {
                            $relation->dissociate();
                        }
                    } elseif (is_a($relation, HasOne::class)) {
                        if ($newAssoc) {
                            $newData = [ 'id' => $newAssoc ];
                            $this->saveRelatedItem(
                                $this->model,
                                $relationName,
                                $newData,
                                $request
                            );
                        } else {
                            $this->deleteRelatedItem($this->model, $relationName, $request);
                        }
                    }
                }
            } elseif (in_array($field, $this->model->items) && is_array($data[$field])) {
                $camelField = Str::camel($field);

                if ($this->model->{$camelField}) {
                    $this->before[$field] = $this->model->{$camelField}->toArray();
                }

                $related = $this->saveRelatedItem($this->model, $field, $data[$field], $request);

                $this->after[$field] = $related->toArray();

                $relation = $this->model->{$camelField}();

                if ($this->model->{$camelField}
                    && $this->model->{$camelField}->id !== $related->id) {
                    if (is_a($relation, HasOne::class)) {
                        $this->model->{$camelField}[
                            $this->model->{$camelField}()->getForeignKeyName()
                        ] = null;
                        $this->model->{$camelField}->save();
                    } else {
                        $relation->dissociate();
                    }
                }

                foreach (array_keys($related->morphOnes) as $morphOne) {
                    $this->savePolymorphicRelation($related, $morphOne, $data[$field]);
                }

                if (is_a($relation, BelongsTo::class)) {
                    $relation->associate($related);
                }
            } elseif (in_array($field, array_keys($this->model->morphOnes))
                && array_key_exists($field, $data)) {
                if (is_array($data[$field]) && !!$data[$field]) {
                    if ($this->model->{$field}
                        && $this->model->{$field}->id !== $data[$field]['id']) {
                        $this->model->{$field}->delete();
                    }

                    $this->savePolymorphicRelation($this->model, $field, $data);
                } else {
                    $this->deletePolymorphicRelation($this->model, $field);
                }
            }
        }
    }

    protected function saveCollections($data, $request) {
        foreach (array_diff(array_keys($data), $this->model->getFillable()) as $field) {
            if ($field === 'id') {
                continue;
            }

            if (is_array($data[$field])) {
                $existingRelations = array_merge(
                    $this->model->collections,
                    array_keys($this->model->morphManys)
                );

                if (in_array($field, $existingRelations)) {
                    $relation = $this->model->{Str::camel($field)}();

                    if (is_a($relation, HasManyThrough::class)) {
                        continue;
                    }

                    $ids = [];

                    if (method_exists($relation, 'getPivotClass')) {
                        $pivotClass = $relation->getPivotClass();
                        $pivot = new $pivotClass;
                        $pivotAttributes = $pivot->getFillable();

                        $isExtendedPivot = $this->isBaseModel($pivot);
                        if ($isExtendedPivot) {
                            $pivotItems = array_merge(
                                $pivot->items,
                                array_keys($pivot->morphOnes)
                            );
                            $pivotCollections = array_merge(
                                $pivot->collections,
                                array_keys($pivot->morphManys)
                            );
                        } else {
                            $pivotItems = [];
                            $pivotCollections = [];
                        }

                        foreach ($data[$field] as $element) {
                            $pivotData = [];
                            $pivotItemData = [];

                            foreach ($pivotAttributes as $pivotAttribute) {
                                if (!array_key_exists($pivotAttribute, $element)) {
                                    continue;
                                }

                                $pivotData[$pivotAttribute] = $element[$pivotAttribute];
                                unset($element[$pivotAttribute]);
                            }

                            foreach ($pivotItems as $pivotItem) {
                                if (!array_key_exists($pivotItem, $element)) {
                                    continue;
                                }

                                $pivotItemData[$pivotItem] = $element[$pivotItem];
                                unset($element[$pivotItem]);
                            }

                            if (!array_key_exists('id', $element) || !$element['id']) {
                                $pivot->fill($pivotData);
                                $pivot->{$relation->getForeignPivotKeyName()} = $this->model->id;
                                $pivot->save();
                                $element['id'] = $pivot->{$relation->getRelatedPivotKeyName()};
                                $targetPivot = $pivot;
                                $pivot = new $pivotClass();
                            }
                            else
                            {
                                $sync = [];
                                $sync[$element['id']] = $pivotData;
                                $relation->syncWithoutDetaching($sync);

                                $targetPivot = $this->model->{Str::camel($field)}()
                                    ->find($element['id'])
                                    ->pivot;
                            }

                            foreach (array_keys($pivot->morphOnes ?: []) as $pivotItem) {
                                $this->savePolymorphicRelation(
                                    $targetPivot,
                                    $pivotItem,
                                    $pivotItemData
                                );
                            }

                            foreach ($pivotCollections as $pivotCollection) {
                                if (isset($element[$pivotCollection])) {
                                    $targetPivot->{$pivotCollection}()->sync(
                                        array_map(function ($i) {
                                            return $i['id'];
                                        }, $element[$pivotCollection])
                                    );
                                }
                            }

                            $ids[] = $element['id'];
                        }
                    } else {
                        foreach ($data[$field] as $element) {
                            if (array_key_exists('id', $element) && $element['id']) {
                                $ids[] = $element['id'];
                            }
                        }
                    }

                    $relatedClass = $relation->getRelated();

                    if ($relatedClass->readOnly) {
                        continue;
                    }

                    if (is_a($relation, MorphMany::class)) {
                        foreach ($ids as $id) {
                            $item = $relatedClass->find($id);
                            $relation->save($id);
                        }
                        $relation->sync($ids);
                    } elseif (is_a($relation, HasMany::class)) {
                        $newItems = [];
                        $this->before[$field] = [];
                        foreach ($data[$field] as $element) {
                            if (array_key_exists('id', $element) && $element['id']) {
                                $existingItem = $relatedClass->find($element['id']);
                                array_push(
                                    $this->before[$field],
                                    array_diff_key(
                                        $existingItem->toArray(),
                                        array_flip(array_merge(
                                            $relatedClass->items,
                                            $relatedClass->collections,
                                            array_keys($relatedClass->morphOnes),
                                            array_keys($relatedClass->morphManys)
                                        ))
                                    )
                                );
                                $existingItem->fill($element);
                                $foreignKeyName = $relation->getForeignKeyName();
                                $existingItem->{$foreignKeyName} = $this->model->id;
                                $newItems[] = $existingItem;
                            } else {
                                $newItem = new $relatedClass;
                                $newItem->fill($element);
                                $newItems[] = $newItem;
                            }
                        }

                        $existingIds = $relation->get()->pluck('id')->toArray();
                        $removedIds = array_diff($existingIds, $ids);

                        if (!empty($removedIds)) {
                            $removedQuery = $relation->getRelated()->whereIn('id', $removedIds);
                            $this->before[$field] = array_merge(
                                $this->before[$field],
                                $removedQuery->get()->toArray()
                            );
                            $removedQuery->delete();
                        }

                        $savedNewItems = $relation->saveMany($newItems);
                        $this->after[$field] = array_map(function ($i) use ($relatedClass) {
                            return array_diff_key(
                                $i->toArray(),
                                array_flip(array_merge(
                                    $relatedClass->items,
                                    $relatedClass->collections,
                                    array_keys($relatedClass->morphOnes),
                                    array_keys($relatedClass->morphManys)
                                ))
                            );
                        }, $savedNewItems);
                    } elseif (is_a($relation, BelongsToMany::class)) {
                        $relation->sync($ids);
                    }
                }
            }
        }
    }

    protected function deletePolymorphicRelation(&$model, $field) {
        if ($currentRelation = $model->{$field}()->first()) {
            $currentRelation->delete();
        }

        return $currentRelation;
    }

    protected function savePolymorphicRelation(&$model, $field, &$data) {
        if (!array_key_exists($field, $data)) {
            return $model->{$field};
        }

        if (array_key_exists($field, $data)) {
            if (!$data[$field]) {
                if ($model->{$field}) {
                    $model->{$field}->delete();
                }

                return null;
            }
        }

        if (array_key_exists('id', $data[$field])) {
            $newRelation = $model->{$field}()
                ->getRelated()->find($data[$field]['id']);
        } else {
            $newRelation = $model->{$field}()->getRelated();
        }

        if (!$newRelation) {
            return null;
        }

        $morphId = $model->morphOnes[$field] . '_id';
        $morphType = $model->morphOnes[$field] . '_type';

        $newRelation->{$morphId} = $model->id;
        $newRelation->{$morphType} = get_class($model);

        $newRelation->save();

        return $newRelation;
    }

    protected function deleteRelatedItem(&$model, $field, $request) {
        $relation = $model->{$field}();

        $relatedClass = $relation->getRelated();
        if (method_exists($relatedClass, 'scopeAccessibleBy')) {
            $query = $relatedClass->accessibleBy($request->user());
        }
        if (method_exists($relatedClass, 'scopeFor')) {
            $query = $relatedClass->for('update', $request->user());
        }
        $related = $query->first();

        if (!$related) {
            return $relation->firs();
        }

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = null;
        }

        $related->save();

        return $related;
    }

    protected function saveRelatedItem(&$model, $field, &$data, $request) {
        $camelField = Str::camel($field);
        $relation = $model->{$camelField}();
        if (!array_key_exists('id', $data)) {
            $related = $relation->getRelated();
        } else {
            $relatedClass = $relation->getRelated();
            $query = $relatedClass;
            if (method_exists($relatedClass, 'scopeAccessibleBy')) {
                $query = $query->accessibleBy($request->user());
            }
            if (method_exists($relatedClass, 'scopeFor')) {
                $query = $query->for('update', $request->user());
            }
            $related = $query->where('id', $data['id'])->first();
        }

        if (!$related) {
            return $relation->getRelated()->first();
        }

        if ($related->readOnly) {
            return $related;
        }

        $relatedData = array_intersect_key(
            $data,
            array_flip($related->getFillable())
        );

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = $model->id;
        }

        $related->fill($relatedData);
        $related->save();

        return $related;
    }

    // FIXME Virtual columns can mess with count
    private function wrapQueryForRealTotal($connection, $query) {
        $subquery = static::queryToRawSql($query);

        return \DB::connection($connection)->select(
            \DB::connection($connection)
                ->raw("SELECT COUNT(*) AS cnt FROM ($subquery) AS query")
        )[0]->cnt;
    }

    private function isBaseModel($class, $autoload = true) {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return in_array('Molotov\Traits\BaseModel', $traits);
    }
}
